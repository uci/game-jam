extends LinkButton

export(String) var scene_to_load

func _on_EXIT_pressed():
	get_tree().quit()

func _on_Exit2_pressed():
	get_tree().quit()

func _on_START_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Retry_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_level_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
