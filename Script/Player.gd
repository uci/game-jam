extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1000
export (int) var jump_speed = -500


const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
    velocity.x = 0
    if is_on_floor() and Input.is_action_just_pressed('up'):
        velocity.y = jump_speed
    
    if Input.is_action_pressed('right'):
        velocity.x += speed
		
    if Input.is_action_pressed('left'):
        velocity.x -= speed
		
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y > 0:
		$sprite.play("jatuh")
		if velocity.x > 0:
			$sprite.flip_h = false
		else:
			$sprite.flip_h = true
			
	elif velocity.y < 0:
		$sprite.play("lompat")
		if velocity.x > 0:
			$sprite.flip_h = false
		else:
			$sprite.flip_h = true
			
	elif velocity.x != 0:
		$sprite.play("jalan")
		if velocity.x > 0:
			$sprite.flip_h = false
		else:
			$sprite.flip_h = true
			
	else:
		$sprite.play("diri")